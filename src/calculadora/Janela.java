package calculadora;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Janela extends JFrame implements ActionListener{
    
    private double numero = 0;
    private JPanel [] linhas = new JPanel[6];
    private JButton [] botoes = new JButton[19];
    private JLabel disp = new JLabel("00000000",SwingConstants.RIGHT);
    private Font font = new Font("Monospaced", Font.PLAIN, 32);
    
    public Janela() {
        super("Calculadora");
        setSize(400, 600);
        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        add(Box.createHorizontalStrut(10));
        for(int i=0; i<6; i++){
            linhas[i] = new JPanel();
            linhas[i].setLayout(new BoxLayout(linhas[i],
                                    BoxLayout.X_AXIS));
            linhas[i].add(Box.createHorizontalStrut(10));
            add(linhas[i]);
            add(Box.createVerticalStrut(10));
        }
        
        disp.setMaximumSize(new Dimension(99999,99999));
        disp.setBorder(BorderFactory.createEmptyBorder(10,10,0,20));
        disp.setFont(font);
        disp.setText(String.valueOf(numero));
        
        linhas[0].add(disp);
        
        String [] nomes = {"C","±","%","÷",
                           "7","8","9","×",
                           "4","5","6","-",
                           "1","2","3","+",
                           "0",",","=",};
        
        for (int i=0; i<19; i++){
            botoes[i] = new JButton(nomes[i]);
            botoes[i].setMaximumSize(new Dimension(99999,99999));
            botoes[i].setPreferredSize(new Dimension(40, 40));
            botoes[i].setBackground(Color.decode("#DDDDDD"));
            botoes[i].setFont(font);
            botoes[i].addActionListener(this);
            linhas[(i/4)+1].add(botoes[i]);
            linhas[(i/4)+1].add(Box.createHorizontalStrut(10));
        }
        
        botoes[16].setPreferredSize(new Dimension(137,40));
        
        setVisible(true);
    }
    

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "1":
            case "2":
            case "3":
            case "4":
            case "5":
            case "6":
            case "7":
            case "8":
            case "9":
            case "0":
                int n = Integer.valueOf(e.getActionCommand());
                numero = (numero * 10) + n;
                disp.setText(String.valueOf(numero)); 
                break;
        }
    }
}
